# Asymmetric Encryption

Asymmetric Encrypt module provides a Drupal Service for performing
Encryption/Decryption using public and private key pairs. It allows us to
encrypt or decrypt the data in a standardized manner.

## Table of Contents

- [Dependencies](#dependencies)
- [Best Practices](#best-practices)
- [How to Use This Module](#how-to-use-this-module)
- [Maintainers](#maintainers)
- [Supporting Organization](#supporting-organization)


## Dependencies

This module depends on the contributed module "encrypt". We have used this 
module as an API to handle Asymmetric encryption and decryption.


## Best Practices

In order to provide real security, it is highly recommended to follow these best
practices:

### Install Libraries via composer
You can install dependency manually. If not installed automatically.

Please make sure that libsodium is enabled in the php.ini file

MAC: brew install libsodium

Reference: https://paragonie.com/book/pecl-libsodium/read/00-intro.md

```shell
composer require paragonie/halite
```
Enable sodium Library in php.ini file

### Keys Folder Structure
The code will create the key directly.

**Note**: The keys folder must be outside the web directory. Make sure you
have backed up the keys once generated.

### Example folder structure:

`<project name>/keys/web/..`

`<project name>/keys/docroot/..`

### Private and Public Key Generation

```php
use ParagonIE\Halite\KeyFactory;

$seal_keypair = \ParagonIE\Halite\KeyFactory::generateEncryptionKeyPair();
$seal['private'] = $seal_keypair->getSecretKey();
$seal['public'] = $seal_keypair->getPublicKey();
```

### Storing the Keys

```php
KeyFactory::save($seal['private'], 'keys/private.key');
KeyFactory::save($seal['public'], 'keys/public.key');
```

### Fetching the Keys

```php
$private_key = KeyFactory::loadEncryptionSecretKey('keys/private.key');
$public_key = KeyFactory::loadEncryptionPublicKey('keys/public.key');
```


## How to Use This Module

As said, we are using this module as service to encrypt or decrypt the data.

**Eg:**

```php
$service = \Drupal::service('asymmetric_encryption.encrypt_data');
$data = [
    'Drupal' => "Drupal",
    "Encrpt" => "encrypt values",
    'decrypt' => "decryption values",
    "check" => "checking the encryption works fine or not",
];
$data1 = $service->encryptElementsStringConvert($data, 'encrypt');
$dataDecrypt = $service->encryptElementsStringConvert($data1, 'decrypt');
````

```php
echo "<pre>";
print_r($data1);
print_r($dataDecrypt);die;
```

**Output:**
```
Array
(
    [Drupal] => HxWpEUinldkgmcJwXTHG5YmozkQ2VjqPEL0LBFx0oGDh2yvD3d1wscHiEUXNkKkWjbMhsadq
    [Encrpt] => ksiUy-paEHLzjNc179ylIVVOeaG4AOsvE0nGG73SRD2VmWARe-LZBjrHeLrJOsN1Kk1z7YrhLwJ-4KuM0pc=
    [decrypt] => nfkPq-cluAihAN-ZZ6bWuW71QpGcWFvJAQHumLSMsXISuroGtGqnk4KuKvfuGMvc_Mszsj6_j_HVJ8lUhN9oCFI=
    [check] => pbAVeE006r8do7mecVOlr_oIg3QCeSWHLRCU59IDmS36qP5vQigwKV6qHKtKlOj3gtJJaoua6jEuNa0APkdcP5_Z6QNkayorO2sez7XefuLHHIR8b6wPQ3Q=
)
Array
(
    [Drupal] => Drupal
    [Encrpt] => encrypt values
    [decrypt] => decryption values
    [check] => checking the encryption works fine or not
)
```

**Note :
As of now we are supporting single dimensional array, In future
we will enhance the functionality to support multidimensional array
and also for object.**

### Instructions:
As we have encrypted data, we can store the data in drupal database
and when fetching the date we can decrypt the data and display the
information.

If we want to use the asymmetric encryption in node.
we can use `hook_entity_presave()` and `hook_entity_load()`
to display the original data.


## Maintainers

This module is currently maintained by the following individuals:

- Prabu Ela ([PrabuEla](https://www.drupal.org/u/prabuela))
- Joseph Olstad ([joseph.olstad](https://www.drupal.org/u/josepholstad))
- Zeeshan Khan ([zeeshan_khan](https://www.drupal.org/u/zeeshan_khan))

## Supporting Organization

This module is supported by:

[Specbee](https://www.drupal.org/specbee).

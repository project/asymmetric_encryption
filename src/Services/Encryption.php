<?php

namespace Drupal\asymmetric_encryption\Services;

use ParagonIE\Halite\Alerts\HaliteAlert;
use ParagonIE\Halite\Alerts\InvalidKey;
use ParagonIE\Halite\Asymmetric\Crypto;
use ParagonIE\Halite\KeyFactory;
use ParagonIE\HiddenString\HiddenString;

/**
 * Service for Encryption and Decryption of data.
 */
class Encryption {

  /**
   * {@inheritdoc}
   *
   * Checking Dependency library is installed or not.
   *
   * @return string
   *   The error message.
   */
  public function checkDependencies() {
    $error = '';
    if (!class_exists('\ParagonIE\Halite\Asymmetric\Crypto')) {
      $error = 'Halite PHP library is not installed.';
    }
    return $error;
  }

  /**
   * Encrypting the given data.
   *
   * @param string $text
   *   Encrypting text.
   *
   * @return string
   *   Returning encrypted string.
   */
  public function encrypt($text) {

    // Checking given value is null.
    if (is_null($text)) {
      $text = '';
    }

    // Checking the Libraries is installed.
    $libCheck = $this->checkDependencies();

    // Early exist for the library check.
    if (!empty($libCheck)) {
      throw new \Exception($libCheck);
    }

    // Loading the public key from the files.
    try {
      $public_key = KeyFactory::loadEncryptionPublicKey('../keys/public.key');
    }
    catch (InvalidKey $th) {
      throw new \Exception($th);
    }

    // Hidden String Fucntionalities.
    try {
      $text_hidden = new HiddenString($text);
      $encrypted_data = Crypto::seal($text_hidden, $public_key);
    }
    catch (HaliteAlert $e) {
      throw new \Exception($e);
    }
    return $encrypted_data;
  }

  /**
   * Decrypting the given data.
   *
   * @param string $text
   *   The text to decryption.
   *
   * @return string
   *   The decrypted string.
   */
  public function decrypt($text) {
    if (is_null($text)) {
      $text = '';
    }

    // Checking Dependency.
    $libCheck = $this->checkDependencies();
    if (!empty($libCheck)) {
      $this->loggerFactory->error($libCheck);
      return ['error' => '0'];
    }

    // Loading the key from the files.
    try {
      $private_key = KeyFactory::loadEncryptionSecretKey('../keys/private.key');
    }
    catch (InvalidKey $th) {
      throw new \Exception($th);
    }

    // Hidden String Fucntionalities.
    try {
      $text_hidden = $text;
      $decrypted_data = Crypto::unseal($text_hidden, $private_key);
    }
    catch (HaliteAlert $e) {
      throw new \Exception($e);
    }
    return $decrypted_data->getString();
  }

  /**
   * Separating array to single element.
   *
   * @param mixed $data
   *   Encrypting data.
   * @param string $flag
   *   Flag to determin encryption or decryption.
   */
  public function encryptElementsStringConvert($data, $flag) {
    if (!empty($data)) {
      try {
        self::createKey();
        return $this->encryptElements($data, strtolower($flag));
      }
      catch (\EncryptException $th) {
        throw new EncryptException($th);
      }
    }
  }

  /**
   * Making array element into single array - (helper function).
   *
   * @param mixed $encryptingData
   *   Encrypting data.
   * @param string $flag
   *   Flag encrypt, flag decrypt to determin encryption or decryption.
   *
   * @return array
   *   Returnting encyrpted data.
   */
  private function encryptElements($encryptingData, $flag) {

    $data = [];
    foreach ($encryptingData as $element_name => $value) {

      if (is_array($value)) {
        echo $element_name;
        $encrypted_value = $this->encryptChildren($encryptingData[$element_name], $flag);
      }
      else {
        if ($flag == 'encrypt') {
          $encrypted_value = $this->encrypt($value);
        }
        elseif ($flag == 'decrypt') {
          $encrypted_value = $this->decrypt($value);
        }

        // Saving the encrypted values.
        $data[$element_name] = $encrypted_value;
      }
    }
    return $data;
  }

  /**
   * Encrypting/Decryping Child element - (helper function).
   *
   * @param mixed $data
   *   Values for encrypt/decrypt.
   * @param string $flag
   *   The "encrypt" for encryption , "decrypt" for decryption.
   */
  private function encryptChildren($data, $flag) {
    foreach ($data as $key => $value) {
      if (is_array($value)) {
        $this->encryptChildren($data[$key], $flag);
      }
      else {
        if ($flag == 'encrypt') {
          $encrypted_value = $this->encrypt($value);
        }
        elseif ($flag == 'decrypt') {
          $encrypted_value = $this->decrypt($value);
        }
        $data[$key] = $encrypted_value;
      }
    }
  }

  /**
   * Creating Directory to store the keys.
   */
  private static function createKey() {
    // Name of the directory.
    $dir_name = '../keys';

    // Check if the directory with the name already exists.
    if (!is_dir($dir_name)) {

      // Create our directory if it does not exist.
      mkdir($dir_name);

      $seal_keypair = KeyFactory::generateEncryptionKeyPair();
      $seal['private'] = $seal_keypair->getSecretKey();
      $seal['public'] = $seal_keypair->getPublicKey();

      KeyFactory::save($seal['private'], '../keys/private.key');
      KeyFactory::save($seal['public'], '../keys/public.key');
    }
  }

}
